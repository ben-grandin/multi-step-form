import { AppBar, Button } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { ThemeProvider } from "@material-ui/styles";
import React from "react";
import theme from "../theme";


function Success({ prevStep }) {
	return (
		<ThemeProvider theme={theme}>
			<AppBar position="static">
				<Typography variant="h6">
					Success
				</Typography>
			</AppBar>
			<Typography variant="h4">
				Thank your for your submission !
			</Typography>
			<Typography variant="p">
				You will get an email soon !
			</Typography>
			<Button variant="contained"

			        onClick={prevStep}>
				Back
			</Button>
		</ThemeProvider>);
}

export default Success;