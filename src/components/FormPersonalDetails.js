import { AppBar, Button, TextField } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { ThemeProvider } from "@material-ui/styles";
import React from "react";
import theme from "../theme";


const styles = {
	button: {
		margin: "15px",
	},
};

function FormPersonalDetails({ values: { occupation, city, bio }, handleChange, nextStep, prevStep }) {

	return (
		<ThemeProvider theme={theme}>

			<AppBar position="static">
				<Typography variant="h6">
					Personal details
				</Typography>
			</AppBar>
			<TextField label="Enter your occupation"
			           onChange={handleChange("occupation")}
			           defaultValue={occupation}/>
			<br/>
			<TextField label="Enter your city"
			           onChange={handleChange("city")}
			           defaultValue={city}/>
			<br/>
			<TextField label="Enter your bio"
			           onChange={handleChange("bio")}
			           defaultValue={bio}/>
			<br/>

			<Button variant="contained"
			        color="primary"
			        style={styles.button}
			        onClick={nextStep}>
				Continue
			</Button>
			<Button variant="contained"
			        style={styles.button}
			        onClick={prevStep}>
				Back
			</Button>
		</ThemeProvider>);
}

export default FormPersonalDetails;