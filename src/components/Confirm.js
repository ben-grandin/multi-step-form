import { AppBar, Button, ThemeProvider } from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import React from "react";
import theme from "../theme";


const styles = {
	button: {
		margin: "15px",
	},

};

function Confirm({ values, handleChange, nextStep, prevStep }) {

	return (
		<ThemeProvider theme={theme}>
			<AppBar position="static">
				<Typography variant="h6">
					Confirm
				</Typography>
			</AppBar>
			<div>
				<List>
					{Object.entries(values).map(([key, value]) => {
						const keyWithSpace = key.replace(/([A-Z])/g, " $1");
						const keyWithSpaceAndUppercase = keyWithSpace.charAt(0)
								.toUpperCase() +
							keyWithSpace.slice(1);

						return <ListItem key={key} button>
							<ListItemText primary={keyWithSpaceAndUppercase}
							              secondary={value}/>
						</ListItem>;
					})}
				</List>

				<Button variant="contained"
				        color="primary"
				        style={styles.button}
				        onClick={nextStep}>
					Continue
				</Button>
				<Button variant="contained"
				        style={styles.button}
				        onClick={prevStep}>
					Back
				</Button>
			</div>
		</ThemeProvider>);
}

export default Confirm;