# A project with React & Material UI
Made following a youtube tutorial from [Traversy Media](https://www.youtube.com/watch?v=zT62eVxShsY)  

## License
The repo is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
