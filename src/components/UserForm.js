import React, { Component } from "react";
import Confirm from "./Confirm";
import FormPersonalDetails from "./FormPersonalDetails";
import FormUserDetails from "./FormUserDetails";
import Success from "./Success";


const devUser = {
	firstName: "Benjamin",
	lastName: "Grandin",
	email: "bengrandin@hotmail.com",
	occupation: "dev",
	city: "Paris",
	bio: "Traveller",
};
const DEFAULT_STATE = {
	step: 1,
	user: process.env.NODE_ENV === "development" ? devUser : {
		firstName: "",
		lastName: "",
		email: "",
		occupation: "",
		city: "",
		bio: "",
	},
};

class UserForm extends Component {
	state = DEFAULT_STATE;

	nextStep = () => {
		this.setState((prevState) => ({
			step: prevState.step + 1,
		}));
	};

	prevStep = () => {
		this.setState((prevState) => ({
			step: prevState.step - 1,
		}));
	};

	handleChange = input => ({ target }) => {
		this.setState(({ user }) => ({
			user: {
				...user,
				[input]: target.value,
			},
		}));
	};

	render() {
		console.log(process.env);
		const { step, user, user: { firstName, lastName, email, occupation, city, bio } } = this.state;

		switch ( step ) {
			case 1:
				return (
					<FormUserDetails nextStep={this.nextStep}
					                 handleChange={this.handleChange}
					                 values={{ firstName, lastName, email }}/>
				);

			case 2:
				return (
					<FormPersonalDetails nextStep={this.nextStep}
					                     prevStep={this.prevStep}
					                     handleChange={this.handleChange}
					                     values={{ occupation, city, bio }}/>
				);
			case 3:
				return (
					<Confirm nextStep={this.nextStep}
					         prevStep={this.prevStep}
					         handleChange={this.handleChange}
					         values={user}/>
				);

			case 4:
				return (
					<Success prevStep={this.prevStep}/>
				);
			default:
				return <p>Step is invalid</p>;

		}

	}
}

export default UserForm;